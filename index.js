"use strict";
var express = require("express");
var bodyParser = require("body-parser");
var _ = require("lodash");
var mongoose = require("mongoose");
var node_fetch_1 = require("node-fetch");
var cors = require("cors");
var app = express();
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost/SkillBranchPC");
app.use(cors({
    origin: "http://account.skill-branch.ru"
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
var pcSchema = new mongoose.Schema({
    board: Object,
    ram: Object,
    os: String,
    floppy: Number,
    hdd: Array,
    monitor: String,
    length: Number,
    height: Number,
    width: Number
});
var PC = mongoose.model("PC", pcSchema);
var newPC;
node_fetch_1["default"]("https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json")
    .then(function (res) {
    return res.json();
})
    .then(function (json) {
    newPC = new PC(json);
    newPC.save(function (err) {
        if (err) {
            throw Error("PC information not saved");
        }
        console.log("PC information updated");
    });
});
app.all("/task3A/volumes", function (req, res) {
    var result = {};
    _.each(newPC.hdd, function (hdd) {
        if (typeof result[hdd.volume] === "undefined") {
            result[hdd.volume] = 0;
        }
        result[hdd.volume] += hdd.size;
    });
    result = _.mapValues(result, function (v) {
        return v + "B";
    });
    res.json(result);
});
app.all(/^\/task3A\/?.*$/, function (req, res) {
    console.log(req.url);
    var parts = _.trim(req.url, "/").split("/").slice(1);
    try {
        var result = {
            board: newPC.board,
            ram: newPC.ram,
            os: newPC.os,
            floppy: newPC.floppy,
            hdd: newPC.hdd,
            monitor: newPC.monitor,
            length: newPC.length,
            height: newPC.height,
            width: newPC.width
        };
        while (parts.length) {
            var part = parts.shift();
            if (_.isString(result)) {
                res.status(404).send("Not Found");
                return;
            }
            else if (result.hasOwnProperty(part) && !_.isArray(result) && typeof result[part] !== "undefined") {
                result = result[part];
            }
            else if (_.isArray(result) && !isNaN(parseInt(part, 10)) && typeof result[parseInt(part, 10)] !== "undefined") {
                result = result[parseInt(part, 10)];
            }
            else {
                res.status(404).send("Not Found");
                return;
            }
        }
        res.status(200).json(result);
    }
    catch (err) {
        console.log(err);
        res.status(404).send("Not Found");
    }
});
app.all(/.*/, function (req, res) {
    res.status(404).send("Not Found");
});
app.listen(3000, function () {
    console.log("Example app listening on port 3000!");
});
//# sourceMappingURL=index.js.map