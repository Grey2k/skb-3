// tslint:disable-next-line
/// <reference path="types/url-parse.d.ts" />

import express = require("express");
import bodyParser = require("body-parser");
import _ = require("lodash");
import mongoose = require("mongoose");
import fetch from "node-fetch";
import cors = require("cors");

import Request = express.Request;
import Response = express.Response;

import {iPC, iPCHdd} from "./interfaces/pc";

let app = express();

// Use native promises
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost/SkillBranchPC");

// CORS middleware
app.use(cors({
    origin: "http://account.skill-branch.ru",
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true,
}));

// define a model
interface iPCModel extends iPC, mongoose.Document {
}

let pcSchema = new mongoose.Schema({
    board: Object,
    ram: Object,
    os: String,
    floppy: Number,
    hdd: Array,
    monitor: String,
    length: Number,
    height: Number,
    width: Number,
});

let PC = mongoose.model<iPCModel>("PC", pcSchema);
let newPC: iPCModel;

fetch("https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json")
    .then(function (res) {
        return res.json();
    })
    .then(function (json) {
        newPC = new PC(json);
        newPC.save(err => {
            if (err) {
                throw Error("PC information not saved");
            }

            // tslint:disable-next-line
            console.log("PC information updated");
        });
    });

// routes
app.all("/task3A/volumes", function (req: Request, res: Response) {
    let result = <any> {};

    _.each(newPC.hdd, function (hdd: iPCHdd) {
        if (typeof result[hdd.volume] === "undefined") {
            result[hdd.volume] = 0;
        }

        result[hdd.volume] += hdd.size;
    });

    result = _.mapValues(result, function (v) {
        return v + "B";
    });

    res.json(result);
});

app.all(/^\/task3A\/?.*$/, function (req: Request, res: Response) {
    // tslint:disable-next-line
    console.log(req.url);

    let parts = _.trim(req.url, "/").split("/").slice(1);

    try {
        let result = <any> {
            board: newPC.board,
            ram: newPC.ram,
            os: newPC.os,
            floppy: newPC.floppy,
            hdd: newPC.hdd,
            monitor: newPC.monitor,
            length: newPC.length,
            height: newPC.height,
            width: newPC.width,
        };

        while (parts.length) {
            let part = parts.shift();
            if (_.isString(result)) {
                res.status(404).send("Not Found");
                return;
            } else if (result.hasOwnProperty(part) && !_.isArray(result) && typeof result[part] !== "undefined") {
                result = result[part];
            } else if (_.isArray(result) && !isNaN(parseInt(part, 10)) && typeof result[parseInt(part, 10)] !== "undefined") {
                result = result[parseInt(part, 10)];
            } else {
                res.status(404).send("Not Found");
                return;
            }
        }
        res.status(200).json(result);
    } catch (err) {
        // tslint:disable-next-line
        console.log(err);
        res.status(404).send("Not Found");
    }
});

app.all(/.*/, function (req: Request, res: Response) {
    res.status(404).send("Not Found");
});

app.listen(3000, function () {
    // tslint:disable-next-line
    console.log("Example app listening on port 3000!");
});
