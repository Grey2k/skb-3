interface iPCBoardCPU {
    model: string,
    hz: number
}

interface iPCBoard {
    vendor: string,
    model: string,
    cpu: iPCBoardCPU,
    image: string,
    video: string
}

interface iPCRam {
    vendor: string,
    volume: number,
    pins: number
}

interface iPCHdd {
    vendor: string,
    size: number,
    volume: string
}

interface iPC {
    board: iPCBoard,
    ram: iPCRam,
    os: string,
    floppy: number,
    hdd: Array<iPCHdd>,
    monitor?: string,
    length: number,
    height: number,
    width: number
}

export {iPC, iPCHdd}